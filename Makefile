build:
	CGO_ENABLED=0 go build -o ./bin/scaffold ./cmd/scaffold

test:
	go clean -testcache
	go test -v -race ./...

watch:
	modd
	
deps:
	go get -u golang.org/x/tools/cmd/godoc
	go get -u github.com/cortesi/modd/cmd/modd
	go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

lint:
	golangci-lint run --tests=false --enable-all

vendor:
	go mod vendor

release: clean vendor
	./misc/script/release

gitea-release:
	./misc/script/gitea-release

clean:
	rm -rf ./bin ./release ./coverage

.PHONY: test clean lint coverage doc release vendor