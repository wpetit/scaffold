package main

import (
	"fmt"
	"os"

	"forge.cadoles.com/wpetit/scaffold/internal/command"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Usage:    "generate/update directory tree from template",
		Commands: command.All(),
	}

	if err := app.Run(os.Args); err != nil {
		fmt.Printf("%+v\n", err)
		os.Exit(1)
	}
}
