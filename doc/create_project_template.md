# Créer un nouveau modèle de projet

Un modèle de projet `scaffold` est un simple répertoire. Celui ci peut être versionné avec Git et utilisé de manière distante par la commande `scaffold from`.

## Fichiers gabarits ("templates")

Tout fichier dont l'extension est `.gotpl` se verra transformé avec la librairie [`text/template`](https://golang.org/pkg/text/template/) avant d'être copié.

L'extension `.gotpl` sera supprimée avant la copie (_par exemple, un fichier `index.html.gotpl` sera transformé puis copié en `index.html`_).

Les variables déclarées dans le fichier [`scaffold.yml`](./scaffold_file_format.md) seront accessibles à tous les fichiers gabarits.

Afin de faciliter l'écriture des gabarits, `scaffold` intègre la librairie d'extension [`github.com/Masterminds/sprig`](http://masterminds.github.io/sprig/).

## Déclaration de variables et contraintes de validation

Voir le format du fichier [`scaffold.yml`](./scaffold_file_format.md).

> TODO