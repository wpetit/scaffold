## Format du fichier `scaffold.yml`

Le fichier `scaffold.yml` est au format [YAML](https://yaml.org/). C'est ce fichier qui permet de définir les données à injecter dans les modèles lors de la génération d'un nouveau projet avec la commande `scaffold from ...`.

**Schéma**

``` yaml
# Version du format de fichier. Actuellement, la seule valeur possible est 1.
version: 1

# Liste des variables à injecter dans les modèles de fichier
vars:
    - <Var> 
#   - <Var>...
```

## Types

### `<Var>`

Définition d'une variable injectable dans les modèles de fichier.

**Schéma**

```yaml
# Type de la variable. Actuellement, la seul valeur possible est "string"
type: "string"

# Nom de la variable. C'est le nom qui permettra d'accéder à la valeur de la variable dans les modèles de fichiers.
name: <string>

# Description de la variable. C'est le texte qui sera affiché dans l'invite de commande au moment de la génération de l'arborescence de fichiers.
description: <string>

# Liste des contraintes appliquées à la valeur de la variable. La valeur est considérée comme invalide tant que l'ensemble des contraintes ne sont pas remplies.
constraints: 
    - <Constraint>
#   - <Constraint>...
```

### `<Constraint>`

Définition d'une contrainte appliquée à la valeur d'une variable.

```yaml
# Règle de validation à appliquer à la valeur entrée par l'utilisateur.
rule: <Rule>

# Message à afficher à l'utilisateur si la règle de validation concorde avec la valeur entrée par celui ci.
message: <string>
```

**Exemple**

```yaml
# Règle validant que la valeur ne doit pas être vide.
rule: Input == ""
message: Cette valeur ne peut être vide.
```

### `<Rule>`

Règle de validation d'une contrainte appliquée à une variable. 

Ce n'est ni plus ni moins qu'une condition booléenne écrite avec la syntaxe proposée par la librairie [github.com/antonmedv/expr](https://github.com/antonmedv/expr/blob/master/docs/Language-Definition.md).

Voici les variables globales disponibles aux règles:

|Variable|Description|
|--------|-----------|
|`Input` |Valeur entrée par l'utilisateur|