package template

type Option struct {
	IgnorePatterns    []string
	TemplateData      Data
	TemplateExt       string
	Unnattended       bool
	PreferredStrategy Strategy
}
