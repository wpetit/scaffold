package template

import "github.com/pkg/errors"

var (
	ErrUnexpectedOverwriteStrategy = errors.New("unexpected overwrite strategy")
)
