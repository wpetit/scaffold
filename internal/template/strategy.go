package template

import (
	"fmt"

	"github.com/manifoldco/promptui"
	"github.com/pkg/errors"
)

type Strategy int

const (
	Undefined Strategy = -1
	Overwrite Strategy = iota
	CopyAsDist
	DoNotCopy
)

// nolint: gochecknoglobals
var defaultStrategy = CopyAsDist

func askOverwriteStrategy(dst string) (Strategy, bool, error) {
	strategyPrompt := promptui.Select{
		Label: fmt.Sprintf("The file '%s' already exists. What do you want to do ?", dst),
		Items: []string{
			"Leave <dest> file as is",
			"Copy source file as '<dest>.dist'",
			"Overwrite '<dest>' with source file",
		},
	}

	strategySelectedIndex, _, err := strategyPrompt.Run()
	if err != nil {
		return -1, false, errors.Wrap(err, "could not prompt for overwrite strategy")
	}

	asPreferredStrategyPrompt := promptui.Select{
		Label: "Do you want to apply this strategy to all files ?",
		Items: []string{
			"Yes",
			"No",
		},
	}

	preferredSelectedIndex, _, err := asPreferredStrategyPrompt.Run()
	if err != nil {
		return -1, false, errors.Wrap(err, "could not prompt for preferred strategy")
	}

	useAsPreferredStrategy := preferredSelectedIndex == 0

	switch strategySelectedIndex {
	case 0:
		return DoNotCopy, useAsPreferredStrategy, nil
	case 1:
		return CopyAsDist, useAsPreferredStrategy, nil
	case 2:
		return Overwrite, useAsPreferredStrategy, nil
	default:
		return -1, false, ErrUnexpectedOverwriteStrategy
	}
}
