package project

import (
	"net/url"
	"strings"

	"gopkg.in/src-d/go-billy.v4/osfs"

	"gopkg.in/src-d/go-billy.v4"
)

const LocalScheme = "local"
const FileScheme = "file"

type LocalFetcher struct{}

func (f *LocalFetcher) Fetch(url *url.URL) (billy.Filesystem, error) {
	return osfs.New(url.Path), nil
}

func (f *LocalFetcher) Match(url *url.URL) bool {
	if url.Scheme == LocalScheme || url.Scheme == FileScheme {
		return true
	}

	if url.Scheme == "" && isFilesystemPath(url.Path) {
		return true
	}

	return false
}

func NewLocalFetcher() *LocalFetcher {
	return &LocalFetcher{}
}

func isFilesystemPath(path string) bool {
	return strings.HasPrefix(path, ".") || strings.HasPrefix(path, "/")
}
