package project

import (
	"net/url"
	"testing"
)

func TestGitFetcher(t *testing.T) {
	git := NewGitFetcher()

	projectURL, err := url.Parse("forge.cadoles.com/wpetit/goweb")
	if err != nil {
		t.Fatal(err)
	}

	fs, err := git.Fetch(projectURL)
	if err != nil {
		t.Fatal(err)
	}

	if fs == nil {
		t.Fatal("fs should not be nil")
	}
}

func TestGitMatch(t *testing.T) {
	testCases := []struct {
		RawURL      string
		ShouldMatch bool
	}{
		{"git://wpetit/scaffold", true},
		{"forge.cadoles.com/wpetit/scaffold", true},
	}

	git := NewGitFetcher()

	for _, tc := range testCases {
		func(rawURL string, shouldMatch bool) {
			t.Run(rawURL, func(t *testing.T) {
				projectURL, err := url.Parse(rawURL)
				if err != nil {
					t.Fatal(err)
				}

				if e, g := shouldMatch, git.Match(projectURL); g != e {
					t.Errorf("git.Match(url): expected '%v', got '%v'", e, g)
				}
			})
		}(tc.RawURL, tc.ShouldMatch)
	}
}
