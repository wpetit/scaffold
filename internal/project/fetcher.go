package project

import (
	"net/url"

	"gopkg.in/src-d/go-billy.v4"
)

type Fetcher interface {
	Match(*url.URL) bool
	Fetch(*url.URL) (billy.Filesystem, error)
}
