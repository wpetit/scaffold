package project

import (
	"net/url"
	"testing"
)

func TestLocalMatch(t *testing.T) {
	testCases := []struct {
		RawURL      string
		ShouldMatch bool
	}{
		{"local://wpetit/scaffold", true},
		{"./forge.cadoles.com/wpetit/scaffold", true},
	}

	local := NewLocalFetcher()

	for _, tc := range testCases {
		func(rawURL string, shouldMatch bool) {
			t.Run(rawURL, func(t *testing.T) {
				projectURL, err := url.Parse(rawURL)
				if err != nil {
					t.Fatal(err)
				}

				if e, g := shouldMatch, local.Match(projectURL); g != e {
					t.Errorf("local.Match(url): expected '%v', got '%v'", e, g)
				}
			})
		}(tc.RawURL, tc.ShouldMatch)
	}
}
