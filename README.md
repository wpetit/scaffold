# Scaffold

## Description

`scaffold` est un outil permettant de générer une arborescence de fichiers à partir d'un modèle pré-existant.

Ce modèle peut être soit un répertoire local soit un dépôt Git distant.

De plus, `scaffold` intègre un mécanisme de templating permettant d'injecter des données dynamiques dans les fichiers à copier. Par défaut, tout fichier dont l'extension est `.gotpl` sera soumis à ce templating.

Un fichier "manifeste" (par défaut `scaffold.yml`) peut être créé afin de déclarer les variables à injecter. Ces variables seront demandées à l'utilisateur lors du lancement de la commande `scaffold from`.

## Installation

### À partir des sources

```
go install forge.cadoles.com/wpetit/scaffold/cmd/scaffold
```

### À partir des binaires

1. [Télécharger la dernière version du binaire correspondant à votre plateforme](https://forge.cadoles.com/wpetit/scaffold/releases) (dernière version stable: 0.0.2)
2. Extraire l'archive et place le binaire `bin/scaffold` dans votre `$PATH` (par exemple dans le répertoire `/usr/local/bin`)

## Usage

```
NAME:
   scaffold - generate/update directory tree from template

USAGE:
   scaffold [global options] command [command options] [arguments...]

COMMANDS:
   from, n  generate a new project from a given template url
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h  show help (default: false)
```

### Commandes

#### `from`

```
NAME:
   scaffold from - generate a new project from a given template url

USAGE:
   scaffold from [command options] <URL>

OPTIONS:
   --directory DIR, -d DIR   Set destination to DIR (default: "./")
   --manifest FILE, -m FILE  The scaffold manifest FILE (default: "scaffold.yml")
   --ignore value, -i value  Ignore files matching pattern (default: ".git", ".git/**", "./.git/**/*")
   --help, -h                show help (default: false)
```

## Documentation

- [Créer un modèle de projet](./doc/create_project_template.md)
- [Format du fichier `scaffold.yml`](./doc/scaffold_file_format.md)

## Licence

[GPL-3.0](./LICENSE)